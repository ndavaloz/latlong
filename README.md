# IPv4 Latitude/Longitude

## Description

For this project, we are outputting the longitude and latitude coordinates for a given (valid) IPv4 address. 

## Installation

1. If not already installed, download [Docker](https://www.docker.com/products/docker-desktop) (You will need a docker account)
2. Clone repo from [Insert Repo Name](www.github.com/tndavaloz/latlong)
3. Setup

### Local Development without Docker

* `npm install`
* `npm run build`
* `npm start`

### Docker Setup

* `docker build .`
* `docker-compose up`

Visit localhost:3000 to input your IPv4 Address

## Running Tests

* `npm run test`

## To Do's

1. Implement a logger for errors
2. Set-up some sort of DI solution, possibly using [BottleJS](https://github.com/young-steveo/bottlejs)
3. Create Interface for Reader class and inject into service (more easily testable)
4. Utilize Express Router and possibly template language for more customization and clean routing
5. Move GeoLite2 data out of data/. Ideally into a db or setup a simple service with caching setup to reduce lookup times.
