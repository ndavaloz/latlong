import { Coordinates, LocationService } from "../../src/Location/Service";
import * as path from "path";

const filePath = path.resolve('src', 'data', './GeoLite2-City.mmdb')
const ip = "128.101.101.101";

it("should return the correct coordinates", async () => {
  const service = new LocationService(ip, filePath);

  const expected: Coordinates = {
    latitude: 45.0782,
    longitude: -93.1894,
  };

  const result = await service.getLatitudeLongitude();
  expect(result).toEqual(expected);
});

it("should set error property", async () => {
  const service = new LocationService("2.3.3", filePath);

  const expected: Coordinates = {
    latitude: null,
    longitude: null,
    error: "Error with protocol given: 2.3.3. Please enter valid address.",
  };

  const result = await service.getLatitudeLongitude();
  expect(result).toEqual(expected);
});