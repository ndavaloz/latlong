import express from "express";
import { LocationService } from "./Location/Service";
import * as path from "path";

const app = express();
const PORT = 3000;

app.use(express.urlencoded());

app.get('/', function (req: express.Request, res: express.Response) {
  res.end(
    `
        <!doctype html>
        <html>
        <body>
            <form action="/data" method="post">
                <input type="text" name="protocol" placeholder="Enter IPv4 Address"/><br />
                <button>Get Coordinates!</button>
            </form>
        </body>
        </html>
    `
  );
});

app.post('/data', async (req: express.Request, res: express.Response) => {
  const filePath = path.resolve('src', 'data', './GeoLite2-City.mmdb');

  const location = new LocationService(req.body.protocol, filePath);
  const latLong = await location.getLatitudeLongitude();

  res.send(latLong);
});

app.listen(PORT, () => {
  console.log('Server is running on PORT:', PORT);
});