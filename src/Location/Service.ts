import { Reader, LocationRecord } from '@maxmind/geoip2-node';

export interface Coordinates {
  latitude: number | null,
  longitude: number | null,
  error?: string | null,
};

export class LocationService {
  private ip: string;
  private filePath: string;

  public constructor(ip: string, filePath: string) {
    this.ip = ip;
    this.filePath = filePath;
  }

  public async getLatitudeLongitude(): Promise<Coordinates> {
    const response = await Reader.open(this.filePath);
    let coordinates: Coordinates = {
      latitude: null,
      longitude: null,
      error: null,
    };

    try {
    const city = response.city(this.ip);
    const location = city.location as LocationRecord;

    coordinates = {
      longitude: location.longitude,
      latitude: location.latitude,
    }
    } catch (e) {
      coordinates.error = `Error with protocol given: ${this.ip}. Please enter valid address.`;
    }

    return coordinates;
  }
}
